package cz.uhk.smartreminder.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import cz.uhk.smartreminder.R;
import cz.uhk.smartreminder.model.repository.TaskListRepository;
import cz.uhk.smartreminder.model.repository.TaskRepository;
import cz.uhk.smartreminder.model.repository.entities.TaskList;

public class TaskListActivity extends ProtectedActivity {

    TaskList selectedTaskList;
    EditText taskListName;
    int id;
    final static int MENU_ITEM_ID_SAVE = 0, MENU_ITEM_ID_CANCEL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);

        Intent i = getIntent();
        id = i.getIntExtra("taskListId", -1);
        selectedTaskList = id == -1 ? null : new TaskListRepository(this).getTaskListById(id);
        taskListName = findViewById(R.id.task_list_name);

        if (id == -1) {
            setTitle(R.string.title_activity_task_list_add);
        } else {
            taskListName.setText(selectedTaskList.getName());
            setTitle(R.string.title_activity_task_list_edit);
        }

        taskListName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    saveTaskList();
                }

                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        redirectToMainActivity();
    }

    private void saveTaskList() {
        TaskListRepository taskListRepository = new TaskListRepository(this);

        if (TextUtils.isEmpty(taskListName.getText())) {
            Toast.makeText(TaskListActivity.this, getString(R.string.msg_task_list_name_is_empty), Toast.LENGTH_SHORT).show();
            return;
        }

        if (selectedTaskList == null) {
            taskListRepository.insertTaskList(new TaskList(id, (int) (System.currentTimeMillis() / 1000), taskListName.getText().toString()));
        } else {
            selectedTaskList.setName(taskListName.getText().toString());
            taskListRepository.updateTaskList(selectedTaskList);
        }

        redirectToMainActivity();
    }

    private void redirectToMainActivity() {
        Intent i = new Intent(TaskListActivity.this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        menu.add(Menu.NONE, MENU_ITEM_ID_SAVE, Menu.NONE, null).setIcon(R.mipmap.tick).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        menu.add(Menu.NONE, MENU_ITEM_ID_CANCEL, Menu.NONE, null).setIcon(R.mipmap.cancel).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case MENU_ITEM_ID_SAVE:
                saveTaskList();
                break;

            case MENU_ITEM_ID_CANCEL:
                redirectToMainActivity();
                break;
        }

        return true;
    }

}
