package cz.uhk.smartreminder.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cz.uhk.smartreminder.R;
import cz.uhk.smartreminder.model.components.TaskListsAdapter;
import cz.uhk.smartreminder.model.repository.TaskListRepository;
import cz.uhk.smartreminder.model.repository.TaskRepository;
import cz.uhk.smartreminder.model.repository.entities.Task;
import cz.uhk.smartreminder.model.repository.entities.TaskList;
import cz.uhk.smartreminder.model.services.geofence.GeofenceMethods;

public class MainActivity extends ProtectedActivity {

    static final int MENU_ITEM_ID_ADD = 0;
    static final int MENU_LIST_ITEM_ID_EDIT = 0;
    static final int MENU_LIST_ITEM_ID_DELETE = 1;
    List<TaskList> lstTaskLists = new ArrayList<>();
    ListView lstViewTaskLists;
    TaskListRepository taskListRepository;
    TaskRepository taskRepository;
    GeofenceMethods geofenceMethods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(R.string.title_activity_main);

        lstViewTaskLists = findViewById(R.id.list_view_task_lists);
        taskListRepository = new TaskListRepository(this);
        taskRepository = new TaskRepository(this);

        refreshList();

        geofenceMethods = new GeofenceMethods(this, this);

        geofenceMethods.checkPermission();

        lstViewTaskLists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent i = new Intent(MainActivity.this, TasksActivity.class);
                i.putExtra("taskListId", lstTaskLists.get(position).getId());
                startActivity(i);
            }
        });

        lstViewTaskLists.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {
                final TaskList taskList = lstTaskLists.get(position);
                final int taskListId = taskList.getId();
                PopupMenu popupMenu = new PopupMenu(MainActivity.this, view);

                final Menu menu = popupMenu.getMenu();
                menu.add(Menu.NONE, MENU_LIST_ITEM_ID_EDIT, Menu.NONE, R.string.btn_edit);
                menu.add(Menu.NONE, MENU_LIST_ITEM_ID_DELETE, Menu.NONE, R.string.btn_delete);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case MENU_LIST_ITEM_ID_EDIT:
                                Intent i = new Intent(MainActivity.this, TaskListActivity.class);
                                i.putExtra("taskListId", taskListId);
                                startActivity(i);
                                break;
                            case MENU_LIST_ITEM_ID_DELETE:
                                taskRepository.getTasksByListId(taskListId);
                                taskListRepository.deleteTaskListById(taskListId);
                                List<String> geofenceIds = new ArrayList<>();

                                for(Task t : taskList.getLstTasks()) {
                                    geofenceIds.add(t.getGeofenceId());
                                }

                                geofenceMethods.removeGeofences(geofenceIds);
                                refreshList();
                                break;
                        }
                        return false;
                    }
                });

                popupMenu.show();

                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, MENU_ITEM_ID_ADD, Menu.NONE, null).setIcon(R.mipmap.add).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case MENU_ITEM_ID_ADD:
                Intent i = new Intent(MainActivity.this, TaskListActivity.class);
                startActivity(i);
                break;
        }

        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        geofenceMethods.connectApi();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!geofenceMethods.checkPermission()) {
            lstTaskLists.clear();
            redrawList();
            return;
        }

        refreshList();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        geofenceMethods.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void refreshList() {
        lstTaskLists = taskListRepository.getTaskLists();
        redrawList();
    }

    private void redrawList() {
        lstViewTaskLists.setAdapter(new TaskListsAdapter(this, lstTaskLists));
        lstViewTaskLists.invalidate();
        ((BaseAdapter) lstViewTaskLists.getAdapter()).notifyDataSetChanged();
    }

}
