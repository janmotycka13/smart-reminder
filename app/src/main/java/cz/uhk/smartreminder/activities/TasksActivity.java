package cz.uhk.smartreminder.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;

import java.io.Serializable;

import cz.uhk.smartreminder.R;
import cz.uhk.smartreminder.model.components.TasksSectionsPagerAdapter;
import cz.uhk.smartreminder.model.repository.TaskListRepository;
import cz.uhk.smartreminder.model.repository.TaskRepository;
import cz.uhk.smartreminder.model.repository.entities.TaskList;

public class TasksActivity extends ProtectedActivity {

    static final int MENU_ITEM_ID_ADD = 0;
    TaskList taskList;
    TaskListRepository taskListRepository;
    TaskRepository taskRepository;
    int taskListId;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);

        taskListRepository = new TaskListRepository(this);
        taskRepository = new TaskRepository(this);

        taskListId = getIntent().getIntExtra("taskListId", -1);

        taskList = taskListRepository.getTaskListById(taskListId);
        taskList.setLstTasks(taskRepository.getTasksByListId(taskListId));

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(taskList.getName());
        setSupportActionBar(toolbar);

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);

        String[] titles = new String[] { getString(R.string.title_fragment_uncompleted_tasks), getString(R.string.title_fragment_completed_tasks) };
        mViewPager.setAdapter(new TasksSectionsPagerAdapter(getSupportFragmentManager(), titles, taskListId));

        TabLayout tabLayout = findViewById(R.id.tabs);

        for(int i = 0; i < titles.length; i++) {
            TabLayout.Tab tab = tabLayout.newTab();
            tab.setText(titles[i]);

            tabLayout.addTab(tab);
        }

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        menu.add(Menu.NONE, MENU_ITEM_ID_ADD, Menu.NONE, null).setIcon(R.mipmap.add).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case MENU_ITEM_ID_ADD:
                redirectToTaskActivity(null);
                break;
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshData();
    }

    private void refreshData() {
        mViewPager.getAdapter().notifyDataSetChanged();
        mViewPager.invalidate();
    }

    private void redirectToTaskActivity(@Nullable Integer taskId) {
        Intent i = new Intent(TasksActivity.this, TaskActivity.class);
        i.putExtra("taskListId", taskListId);
        i.putExtra("taskId", taskId == null ? -1 : taskId);
        startActivity(i);
    }
}
