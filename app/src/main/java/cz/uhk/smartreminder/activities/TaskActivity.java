package cz.uhk.smartreminder.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.smartreminder.R;
import cz.uhk.smartreminder.model.repository.SettingsRepository;
import cz.uhk.smartreminder.model.repository.TaskRepository;
import cz.uhk.smartreminder.model.repository.entities.Task;
import cz.uhk.smartreminder.model.services.geofence.GeofenceMethods;
import cz.uhk.smartreminder.model.services.geofence.GeofenceMonitorService;

public class TaskActivity extends ProtectedActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener, ResultCallback<Status> {

    GoogleMap map;
    GoogleApiClient googleApiClient;
    Location lastKnownLocation;
    MapFragment mapFragment;
    PendingIntent pendingIntent;
    final int REQUEST_PERMISSION = GeofenceMethods.REQUEST_PERMISSION, UPDATE_INTERVAL = GeofenceMethods.UPDATE_INTERVAL /* IN MILISEC */, FASTEST_INTERVAL = GeofenceMethods.FASTEST_INTERVAL /* IN MILISEC */, MENU_ITEM_ID_SAVE = 0, MENU_ITEM_ID_CANCEL = 1;
    String TAG = TaskActivity.class.getSimpleName();
    LocationRequest locationRequest;
    Marker actualLocationMarker, geofenceLocationMarker;
    EditText notes, taskName;
    GeofenceMethods geofenceMethods;
    TaskRepository taskRepository;
    int taskListId, taskId;
    final static float MAP_ZOOM = 14f;
    Dialog mapDlg;
    CheckBox chboxSendNotification, chboxIsCompleted;
    Task editedTask;

    public static Intent makeNotification(Context context, int taskListId, int taskId) {
        Intent i = new Intent(context, TaskActivity.class);
        i.putExtra("taskId", taskId);
        i.putExtra("taskListId", taskListId);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        taskId = getIntent().getIntExtra("taskId", -1);

        mapDlg = new Dialog(this, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        mapDlg.setContentView(R.layout.dlg_map);

        mapDlg.setCanceledOnTouchOutside(false);

        findViewById(R.id.btn_set_location).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapDlg.show();
            }
        });

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent serviceIntent = new Intent(this, GeofenceMonitorService.class);
        pendingIntent = PendingIntent.getService(this, 0, serviceIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        notes = findViewById(R.id.note);
        taskName = findViewById(R.id.title_task_name);
        chboxIsCompleted = findViewById(R.id.chbox_completed);
        chboxSendNotification = findViewById(R.id.chbox_send_notification);

        chboxIsCompleted.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                chboxSendNotification.setChecked(!value);
            }
        });

        chboxSendNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                if (value) chboxIsCompleted.setChecked(!value);
            }
        });

        geofenceMethods = new GeofenceMethods(this, this);
        taskRepository = new TaskRepository(this);

        Intent intent = getIntent();
        taskListId = intent.getIntExtra("taskListId", -1);

        editedTask = taskRepository.getTaskById(this.taskId == -1 ? null : this.taskId);

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                onLocationSelect(place.getLatLng());
            }

            @Override
            public void onError(Status status) {
                Log.i(TAG, "An error occurred: " + status);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        googleApiClient.connect();
        geofenceMethods.connectApi();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getLastKnownLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull Status status) {

    }

    @Override
    public void onLocationChanged(Location location) {
        lastKnownLocation = location;
        writeActualLocation(location);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        writeGeofenceLocation(latLng);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        if (checkPermission()) {
            map.setMyLocationEnabled(true);
            map.getUiSettings().setMyLocationButtonEnabled(true);
        }

        map.setIndoorEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.setOnMapClickListener(this);
        map.setOnMarkerClickListener(this);

        refreshValues();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastKnownLocation();
            } else {

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        menu.add(Menu.NONE, MENU_ITEM_ID_SAVE, Menu.NONE, null).setIcon(R.mipmap.tick).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        menu.add(Menu.NONE, MENU_ITEM_ID_CANCEL, Menu.NONE, null).setIcon(R.mipmap.cancel).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        AlertDialog dlg = new AlertDialog.Builder(TaskActivity.this).create();
        dlg.setTitle(R.string.title_error);

        switch (item.getItemId()) {
            case MENU_ITEM_ID_SAVE:

                if (TextUtils.isEmpty(taskName.getText().toString())) {
                    dlg.setMessage(getString(R.string.msg_no_task_name));
                    dlg.show();
                    break;
                }

                if (geofenceLocationMarker != null) {
                    addGeofence();
                } else {
                    dlg.setMessage(getString(R.string.msg_no_geofence_marker));
                    dlg.show();
                }
                break;

            case MENU_ITEM_ID_CANCEL:
                redirectToTasksActivity();
                break;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        redirectToTasksActivity();
    }

    private void addGeofence() {

        float radius = new SettingsRepository(getApplicationContext()).getSettings().getRadius(); //in meters
        double latitude = geofenceLocationMarker.getPosition().latitude;
        double longitude = geofenceLocationMarker.getPosition().longitude;

        if (taskId != -1) {
            editedTask.setSendNotification(chboxSendNotification.isChecked());
            editedTask.setCompleted(chboxIsCompleted.isChecked());
            editedTask.setName(taskName.getText().toString());
            editedTask.setNote(notes.getText().toString());
            editedTask.setLatitude(latitude);
            editedTask.setLongitude(longitude);

            taskRepository.updateTask(editedTask);

        } else {
            taskRepository.addTask(new Task(0, (int) (System.currentTimeMillis() / 1000), notes.getText().toString(), taskName.getText().toString(), latitude, longitude, chboxSendNotification.isChecked(), taskListId));
        }

        List<Geofence> geofences = new ArrayList<>();
        List<String> taskIds = new ArrayList<>();
        List<Task> tasks = taskRepository.getTasksWithEnableSendingNotifications();

        for (Task t : tasks) {
            taskIds.add(String.valueOf(t.getId()));
            Geofence geofence =  new Geofence.Builder()
                    .setRequestId(t.getGeofenceId())
                    .setCircularRegion(t.getLatitude(), t.getLongitude(), radius)
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                    .build();

            geofences.add(geofence);
        }

        geofenceMethods.removeGeofences(taskIds);
        geofenceMethods.addGeofences(geofences);

        redirectToTasksActivity();
    }

    private void redirectToTasksActivity() {
        Intent i = new Intent(TaskActivity.this, TasksActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("taskListId", taskListId);
        startActivity(i);
    }

    private boolean checkPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void getLastKnownLocation() {
        if (checkPermission()) {
            lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

            if (lastKnownLocation != null) {
                writeLastLocation();
                startLocationUpdates();
            } else {
                startLocationUpdates();
            }

        } else {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION }, REQUEST_PERMISSION);
        }
    }

    private void startLocationUpdates() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        if (checkPermission()) LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    private void writeLastLocation() {
        writeActualLocation(lastKnownLocation);
    }

    private void writeActualLocation(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(String.format("%s, %s", latLng.latitude, latLng.longitude));

        if (map != null) {
            if (actualLocationMarker != null) actualLocationMarker.remove();

            actualLocationMarker = map.addMarker(markerOptions);

            if (geofenceLocationMarker != null) return;
            updateGoogleMapCamera(latLng);
        }
    }

    private void writeGeofenceLocation(LatLng latLng) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        markerOptions.title(String.format("%s, %s", latLng.latitude, latLng.longitude));

        if (map != null) {
            if (geofenceLocationMarker != null) geofenceLocationMarker.remove();
            geofenceLocationMarker = map.addMarker(markerOptions);
        }
    }

    private void updateGoogleMapCamera(LatLng latLng) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, MAP_ZOOM);
        map.animateCamera(cameraUpdate);
    }

    private void refreshValues() {
        TextView textViewCreatedDate = findViewById(R.id.txt_created_date);

        if (editedTask != null) {
            setTitle(editedTask.getName());
            chboxIsCompleted.setVisibility(View.VISIBLE);
            chboxIsCompleted.setChecked(editedTask.isCompleted());
            taskName.setText(editedTask.getName());
            notes.setText(editedTask.getNote());
            chboxSendNotification.setChecked(editedTask.isSendNotification());
            ((Button) findViewById(R.id.btn_set_location)).setText(R.string.title_edit_location);
            onLocationSelect(new LatLng(editedTask.getLatitude(), editedTask.getLongitude()));
            textViewCreatedDate.setVisibility(View.VISIBLE);
            textViewCreatedDate.setText(editedTask.getCompletedDate() == 0 ? String.format("%s %s", getString(R.string.title_created), editedTask.getCreatedDateAsText()) : String.format("%s %s", getString(R.string.title_completed), editedTask.getCompletedDateAsText()));
        } else {
            setTitle(R.string.title_task_add);
            chboxIsCompleted.setVisibility(View.GONE);
            chboxSendNotification.setChecked(true);
        }
    }

    private void onLocationSelect(LatLng latLng) {
        writeGeofenceLocation(latLng);
        updateGoogleMapCamera(latLng);
    }

}
