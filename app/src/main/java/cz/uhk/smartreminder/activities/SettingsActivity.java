package cz.uhk.smartreminder.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.Geofence;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.smartreminder.R;
import cz.uhk.smartreminder.model.repository.SettingsRepository;
import cz.uhk.smartreminder.model.repository.TaskRepository;
import cz.uhk.smartreminder.model.repository.entities.Settings;
import cz.uhk.smartreminder.model.repository.entities.Task;
import cz.uhk.smartreminder.model.repository.entities.TaskList;
import cz.uhk.smartreminder.model.services.database.DatabaseHelper;
import cz.uhk.smartreminder.model.services.geofence.GeofenceMethods;

public class SettingsActivity extends ProtectedActivity {

    static final int MENU_ITEM_ID_SAVE = 0;
    EditText editTextDistance;
    SettingsRepository settingsRepository;
    Settings settings;
    GeofenceMethods geofenceMethods;
    TaskRepository taskRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setTitle(R.string.title_settings);

        taskRepository = new TaskRepository(this);

        settingsRepository = new SettingsRepository(this);
        settings = settingsRepository.getSettings();

        editTextDistance = findViewById(R.id.geofence_distance);
        editTextDistance.setHint(String.format("Minimálně %s m", String.valueOf(DatabaseHelper.DEFAULT_RADIUS)));
        editTextDistance.setText(String.valueOf(settings.getRadius()));
        editTextDistance.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    saveSettings();
                }

                return false;
            }
        });

        geofenceMethods = new GeofenceMethods(this, this);

        geofenceMethods.checkPermission();

    }

    @Override
    protected void onStart() {
        super.onStart();

        geofenceMethods.connectApi();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, MENU_ITEM_ID_SAVE, Menu.NONE, null).setIcon(R.mipmap.tick).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        super.onCreateOptionsMenu(menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case MENU_ITEM_ID_SAVE:
                saveSettings();

                float radius = Float.valueOf(editTextDistance.getText().toString());

                List<Geofence> geofences = new ArrayList<>();
                List<String> taskIds = new ArrayList<>();
                List<Task> tasks = taskRepository.getTasksWithEnableSendingNotifications();

                for (Task t : tasks) {
                    taskIds.add(String.valueOf(t.getId()));
                    Geofence geofence =  new Geofence.Builder()
                            .setRequestId(t.getGeofenceId())
                            .setCircularRegion(t.getLatitude(), t.getLongitude(), radius)
                            .setExpirationDuration(Geofence.NEVER_EXPIRE)
                            .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                            .build();

                    geofences.add(geofence);
                }

                geofenceMethods.removeGeofences(taskIds);
                if (geofences.size() > 0) geofenceMethods.addGeofences(geofences);

                break;
        }

        return true;
    }

    private void saveSettings() {
        if (TextUtils.isEmpty(editTextDistance.getText().toString())) {
            Toast.makeText(SettingsActivity.this, getString(R.string.msg_fill_geofence_distance), Toast.LENGTH_SHORT).show();
            return;
        }

        float value = Float.valueOf(editTextDistance.getText().toString());

        if (value < DatabaseHelper.DEFAULT_RADIUS) {
            Toast.makeText(SettingsActivity.this, getString(R.string.msg_for_min_geofence_distance, String.valueOf(DatabaseHelper.DEFAULT_RADIUS)), Toast.LENGTH_LONG).show();
            return;
        }

        settings.setRadius(value);
        settingsRepository.updateSettings(settings);

        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        geofenceMethods.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
