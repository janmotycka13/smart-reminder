package cz.uhk.smartreminder.model.components;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.smartreminder.R;
import cz.uhk.smartreminder.activities.TaskActivity;
import cz.uhk.smartreminder.model.repository.TaskRepository;
import cz.uhk.smartreminder.model.repository.entities.Task;
import cz.uhk.smartreminder.model.services.geofence.GeofenceMethods;

/**
 * A placeholder fragment containing a simple view.
 */
public class TasksPlaceholderFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    static final String ARG_SECTION_NUMBER = "section_number", ARG_TASK_LIST_ID = "task_list_id";
    static final int MENU_LIST_ITEM_ID_EDIT = 0;
    static final int MENU_LIST_ITEM_ID_DELETE = 1;
    List<Task> tasks;
    List<Task> completedTasks = new ArrayList<>();
    List<Task> incompletedTasks = new ArrayList<>();
    ListView lstViewTasks;
    int sectionNumber, taskListId;
    TaskRepository taskRepository;
    boolean isCreated = false;
    GeofenceMethods geofenceMethods;
    GoogleApiClient googleApiClient;
    LocationRequest locationRequest;
    Location lastKnownLocation;

    public TasksPlaceholderFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static TasksPlaceholderFragment newInstance(int sectionNumber, int taskListId) {
        TasksPlaceholderFragment fragment = new TasksPlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putInt(ARG_TASK_LIST_ID, taskListId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        isCreated = true;
        final View rootView = inflater.inflate(R.layout.fragment_tasks, container, false);
        final Context context = getContext();
        sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER);
        taskListId = getArguments().getInt(ARG_TASK_LIST_ID);
        taskRepository = new TaskRepository(context);
        geofenceMethods = new GeofenceMethods(context, getActivity());

        googleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


        lstViewTasks = rootView.findViewById(R.id.lst_tasks);

        boolean isCompletedListFragment = sectionNumber == 1;

        tasks = taskRepository.getTasksByListId(taskListId);
        completedTasks.clear();
        incompletedTasks.clear();

        for (Task task : tasks) {
            if (task.isCompleted()) completedTasks.add(task);
            else incompletedTasks.add(task);
        }

        lstViewTasks.setAdapter(new TasksAdapter(context, isCompletedListFragment ? completedTasks : incompletedTasks, lstViewTasks, taskListId, isCompletedListFragment, geofenceMethods));
        lstViewTasks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Task taskClicked = (Task) adapterView.getItemAtPosition(position);
                Intent i = new Intent(context, TaskActivity.class);
                i.putExtra("taskId", taskClicked.getId());
                i.putExtra("taskListId", taskListId);
                startActivity(i);
            }
        });
        lstViewTasks.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
                PopupMenu popupMenu = new PopupMenu(getContext(), view);
                final Task taskClicked = (Task) adapterView.getItemAtPosition(position);

                final Menu menu = popupMenu.getMenu();
                menu.add(Menu.NONE, MENU_LIST_ITEM_ID_EDIT, Menu.NONE, R.string.btn_edit);
                menu.add(Menu.NONE, MENU_LIST_ITEM_ID_DELETE, Menu.NONE, R.string.btn_delete);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case MENU_LIST_ITEM_ID_EDIT:
                                Intent i = new Intent(getActivity(), TaskActivity.class);
                                i.putExtra("taskListId", taskListId);
                                i.putExtra("taskId", taskClicked.getId());
                                startActivity(i);
                                break;
                            case MENU_LIST_ITEM_ID_DELETE:
                                List<String> geofenceIds = new ArrayList<>();
                                geofenceIds.add(taskClicked.getGeofenceId());

                                geofenceMethods.removeGeofences(geofenceIds);
                                taskRepository.deleteTask(taskClicked.getGeofenceId());
                                refreshList();
                                break;
                        }
                        return false;
                    }
                });

                popupMenu.show();
                return true;
            }
        });

        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isCreated) {
            refreshList();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        googleApiClient.connect();
        geofenceMethods.connectApi();
    }

    private void refreshList() {
        TasksAdapter adapter = (TasksAdapter) lstViewTasks.getAdapter();

        if (lastKnownLocation != null) {
            adapter.setLastKnownLocation(lastKnownLocation);
        }
        adapter.notifyDataSetChanged();
        lstViewTasks.invalidate();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getLastKnownLocation();
        refreshList();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        lastKnownLocation = location;
        refreshList();
    }

    private void getLastKnownLocation() {
        if (geofenceMethods.checkPermission()) {
            lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

            startLocationUpdates();
        }
    }

    private void startLocationUpdates() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(GeofenceMethods.UPDATE_INTERVAL);
        locationRequest.setFastestInterval(GeofenceMethods.FASTEST_INTERVAL);

        if (geofenceMethods.checkPermission()) LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }
}