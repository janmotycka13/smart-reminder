package cz.uhk.smartreminder.model.utils;


import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.uhk.smartreminder.model.repository.entities.Task;

public class GeofenceUtils {

    public GeofenceUtils() {
    }

    public List<Task> sortByDistance(List<Task> tasks, Location lastKnownLocation) {
        List<Task> list = new ArrayList<>(tasks);
        Collections.sort(list, new SortGeofencesByDistance(new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude())));
        return list;
    }

    class SortGeofencesByDistance implements Comparator<Task> {

        LatLng currentLoc;

        public SortGeofencesByDistance(LatLng currentLoc) {
            this.currentLoc = currentLoc;
        }

        @Override
        public int compare(Task task1, Task task2) {
            double lat1 = task1.getLatitude();
            double lon1 = task1.getLongitude();
            double lat2 = task2.getLatitude();
            double lon2 = task2.getLongitude();

            double distanceToPlace1 = distance(currentLoc.latitude, currentLoc.longitude, lat1, lon1);
            double distanceToPlace2 = distance(currentLoc.latitude, currentLoc.longitude, lat2, lon2);
            return (int) (distanceToPlace1 - distanceToPlace2);
        }

        public double distance(double fromLat, double fromLon, double toLat, double toLon) {
            double radius = 6378137;   // approximate Earth radius, *in meters*
            double deltaLat = toLat - fromLat;
            double deltaLon = toLon - fromLon;
            double angle = 2 * Math.asin( Math.sqrt(
                    Math.pow(Math.sin(deltaLat/2), 2) +
                            Math.cos(fromLat) * Math.cos(toLat) *
                                    Math.pow(Math.sin(deltaLon/2), 2) ) );
            return radius * angle;
        }
    }
}
