package cz.uhk.smartreminder.model.repository.entities;


public class Subtask {

    int id, createdDate, completedDate;
    String name;

    public Subtask(int id, int createdDate, int completedDate, String name) {
        this.id = id;
        this.createdDate = createdDate;
        this.completedDate = completedDate;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public int getCreatedDate() {
        return createdDate;
    }

    public int getCompletedDate() {
        return completedDate;
    }

    public String getName() {
        return name;
    }
}
