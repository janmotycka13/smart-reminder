package cz.uhk.smartreminder.model.services.geofence;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

import cz.uhk.smartreminder.R;
import cz.uhk.smartreminder.activities.TaskActivity;
import cz.uhk.smartreminder.model.repository.TaskRepository;
import cz.uhk.smartreminder.model.repository.entities.Task;


public class GeofenceMonitorService extends Service {

    public static final String ACTION_INIT ="cz.uhk.smartreminder.ACTION_INIT";
    static final String TAG = GeofenceMonitorService.class.getSimpleName();
    NotificationManager notificationManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (ACTION_INIT.equals(intent.getAction())) {
            return START_NOT_STICKY;
        }

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        if (geofencingEvent.hasError()) {
            Log.d(TAG, "Error monitoring region: " + geofencingEvent.getErrorCode());
        } else if (geofencingEvent.getGeofenceTransition() == Geofence.GEOFENCE_TRANSITION_ENTER) {
            sendNotifications(geofencingEvent.getTriggeringGeofences());
        }

        return START_NOT_STICKY;
    }

    private NotificationCompat.Builder getNotificationBuilder(Task task) {
        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(getApplicationContext());
        taskStackBuilder.addParentStack(TaskActivity.class);
        taskStackBuilder.addNextIntent(TaskActivity.makeNotification(getApplicationContext(), task.getTaskListId(), task.getId()));

        PendingIntent notificationPendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.app_icon);
        builder.setContentTitle(getString(R.string.app_name));
        builder.setContentText(task.getName());
        builder.setAutoCancel(true);
        builder.setContentIntent(notificationPendingIntent);
        builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);
        builder.setOngoing(false);

        return builder;
    }

    private void sendNotifications(List<Geofence> geofences) {
        String[] geofenceIds = new String[geofences.size()];
        for (int i = 0; i < geofences.size(); i++) {
            Geofence g = geofences.get(i);
            geofenceIds[i] = g.getRequestId();
        }

        TaskRepository taskRepository = new TaskRepository(getApplicationContext());

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        for (Task t : taskRepository.getTasksByGefenceIds(geofenceIds)) {
            t.setSendNotification(false);
            taskRepository.updateTask(t);

            Notification notification = getNotificationBuilder(t).build();
            notificationManager.notify(Integer.valueOf(t.getGeofenceId()), notification);
        }

    }
}
