package cz.uhk.smartreminder.model.components;

import android.content.Context;
import android.location.Location;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.location.Geofence;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.smartreminder.R;
import cz.uhk.smartreminder.model.repository.SettingsRepository;
import cz.uhk.smartreminder.model.repository.TaskRepository;
import cz.uhk.smartreminder.model.repository.entities.Task;
import cz.uhk.smartreminder.model.services.geofence.GeofenceMethods;
import cz.uhk.smartreminder.model.utils.GeofenceUtils;


public class TasksAdapter extends BaseAdapter {

    Context context;
    List<Task> tasks;
    TaskRepository taskRepository;
    ListView listView;
    int taskListId;
    boolean areAllTasksCompleted;
    GeofenceMethods geofenceMethods;
    Location lastKnownLocation;
    final static String TAG = TasksAdapter.class.getSimpleName();

    public TasksAdapter(@NonNull Context context, List<Task> tasks, @NonNull ListView listView, int taskListId, boolean areAllTasksCompleted, GeofenceMethods geofenceMethods) {
        this.context = context;
        this.tasks = tasks;
        this.listView = listView;
        this.taskListId = taskListId;
        this.areAllTasksCompleted = areAllTasksCompleted;
        this.taskRepository = new TaskRepository(context);
        this.geofenceMethods = geofenceMethods;
    }

    public void setLastKnownLocation(Location lastKnownLocation) {
        this.lastKnownLocation = lastKnownLocation;
    }

    @Override
    public void notifyDataSetChanged() {
        tasks = areAllTasksCompleted ? taskRepository.getCompletedTasks(taskListId) : taskRepository.getIncompletedTasks(taskListId);

        if (lastKnownLocation != null) {
            tasks = new GeofenceUtils().sortByDistance(tasks, lastKnownLocation);
        }

        listView.invalidateViews();
    }

    @Override
    public int getCount() {
        return tasks.size();
    }

    @Override
    public Task getItem(int i) {
        return tasks.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.lst_tasks_item, viewGroup, false);
        }

        final Task task = getItem(i);

        ((TextView) view.findViewById(R.id.txt_task_name)).setText(task.getName());

        CheckBox checkBoxCompleted = view.findViewById(R.id.chbox_tasks_item);
        checkBoxCompleted.setTag(String.format("chboxiscompleted_%s", task.getGeofenceId()));
        checkBoxCompleted.setChecked(task.isCompleted());
        checkBoxCompleted.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                for (Task t : tasks) {
                    if (t.getId() == Integer.valueOf(compoundButton.getTag().toString().split("_")[1])) {
                        t.setSendNotification(!value);
                        t.setCompleted(value);
                        taskRepository.updateTask(t);

                        if (t.isCompleted()) {
                            List<String> geofenceIds = new ArrayList<>();
                            geofenceIds.add(t.getGeofenceId());
                            geofenceMethods.removeGeofences(geofenceIds);

                            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            Ringtone r = RingtoneManager.getRingtone(context, notification);
                            r.play();
                        } else {
                            float radius = new SettingsRepository(context).getSettings().getRadius(); //in meters
                            List<Geofence> geofenceIds = new ArrayList<>();
                            geofenceIds.add(new Geofence.Builder()
                                    .setRequestId(t.getGeofenceId())
                                    .setCircularRegion(t.getLatitude(), t.getLongitude(), radius)
                                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                                    .build());

                            geofenceMethods.addGeofences(geofenceIds);
                        }

                        notifyDataSetChanged();

                        break;
                    }
                }
            }
        });

        return view;
    }
}
