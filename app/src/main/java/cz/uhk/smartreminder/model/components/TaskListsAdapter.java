package cz.uhk.smartreminder.model.components;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.smartreminder.R;
import cz.uhk.smartreminder.model.repository.entities.TaskList;


public class TaskListsAdapter extends BaseAdapter {

    Context context;
    List<TaskList> lstTaskLists = new ArrayList<>();

    public TaskListsAdapter(Context context, List<TaskList> lstTaskLists) {
        this.context = context;
        this.lstTaskLists = lstTaskLists;
    }

    @Override
    public int getCount() {
        return lstTaskLists.size();
    }

    @Override
    public TaskList getItem(int i) {
        return lstTaskLists.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.lst_task_lists_item, null);
        }

        final TaskList taskList = getItem(i);

        ((TextView) view.findViewById(R.id.title_lst_task_lists)).setText(taskList.getName());
        ((TextView) view.findViewById(R.id.created_date)).setText(String.format("%s %s", context.getString(R.string.title_created), taskList.getCreatedDateAsText()));

        TextView taskCount = view.findViewById(R.id.task_count);

        if (taskList.getIncompletedTasksCount() == 0) {
            taskCount.setVisibility(View.GONE);
        } else {
            taskCount.setText(String.valueOf(taskList.getIncompletedTasksCount()));
            taskCount.setVisibility(View.VISIBLE);
        }

        return view;
    }
}
