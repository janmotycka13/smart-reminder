package cz.uhk.smartreminder.model.repository.entities;

import android.content.Intent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Task {

    int id, completedDate, createdDate, taskListId;
    String note, name;
    double latitude, longitude;
    boolean sendNotification = true;
    List<Subtask> lstSubtasks = new ArrayList<>();

    public Task(int completedDate, int createdDate, String note, String name, double latitude, double longitude, boolean sendNotification, int taskListId) {
        this.completedDate = completedDate;
        this.createdDate = createdDate;
        this.note = note;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.sendNotification = sendNotification;
        this.taskListId = taskListId;
    }

    public int getId() {
        return id;
    }

    public int getCompletedDate() {
        return completedDate;
    }

    public int getCreatedDate() {
        return createdDate;
    }

    public String getCreatedDateAsText() {
        Integer i = new Integer(this.createdDate);
        Date d = new Date((long) i * 1000);

        return new SimpleDateFormat("dd.MM.yyyy").format(d);
    }

    public String getCompletedDateAsText() {
        Integer i = new Integer(this.completedDate);
        Date d = new Date((long) i * 1000);

        return new SimpleDateFormat("dd.MM.yyyy").format(d);
    }

    public String getNote() {
        return note;
    }

    public String getName() {
        return name;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getGeofenceId() {
        return String.valueOf(getId());
    }

    public int getTaskListId() {
        return taskListId;
    }

    public boolean isSendNotification() {
        return sendNotification;
    }

    public boolean isCompleted() {
        return getCompletedDate() != 0;
    }

    public List<Subtask> getLstSubtasks() {
        return lstSubtasks;
    }

    public void setLstSubtasks(List<Subtask> lstSubtasks) {
        this.lstSubtasks = lstSubtasks;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCompleted(boolean value) {
        this.completedDate = value ? ((int) (System.currentTimeMillis() / 1000)) : 0;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void addSubtask(Subtask task) {
        lstSubtasks.add(task);
    }

    public void setSendNotification(boolean sendNotification) {
        this.sendNotification = sendNotification;
    }
}
