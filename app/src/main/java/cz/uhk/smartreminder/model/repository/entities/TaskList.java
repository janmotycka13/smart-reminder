package cz.uhk.smartreminder.model.repository.entities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TaskList {

    int id, createdDate;
    String name;
    List<Task> lstTasks = new ArrayList<>();

    public TaskList(int id, int createdDate, String name) {
        this.id = id;
        this.createdDate = createdDate;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public int getCreatedDateAsInteger() {
        return createdDate;
    }

    public String getCreatedDateAsText() {
        Integer i = new Integer(this.createdDate);
        Date d = new Date((long) i * 1000);

        return new SimpleDateFormat("dd.MM.yyyy").format(d);
    }

    public String getName() {
        return name;
    }

    public List<Task> getLstTasks() {
        return lstTasks;
    }

    public void setLstTasks(List<Task> lstTasks) {
        this.lstTasks = lstTasks;
    }

    public int getTasksCount() {
        return lstTasks.size();
    }

    public int getIncompletedTasksCount() {
        int count = 0;
        for (Task t : getLstTasks()) {
            if (!t.isCompleted()) count++;
        }

        return count;
    }

    public void setName(String name) {
        this.name = name;
    }
}
