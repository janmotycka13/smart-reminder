package cz.uhk.smartreminder.model.services.geofence;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.smartreminder.R;
import cz.uhk.smartreminder.model.repository.TaskRepository;

public class GeofenceMethods implements GoogleApiClient.ConnectionCallbacks, LocationListener, GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {

    String TAG = GeofenceMethods.class.getSimpleName();
    Context context;
    GoogleApiClient googleApiClient;
    PendingIntent pendingIntent;
    TaskRepository taskRepository;
    LocationRequest locationRequest;
    Location lastKnownLocation;
    Activity activity;
    public static final int REQUEST_PERMISSION = 999, UPDATE_INTERVAL = 900 /* IN MILISEC */, FASTEST_INTERVAL = 500 /* IN MILISEC */;

    public GeofenceMethods(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;

        Intent i = new Intent(context, GeofenceMonitorService.class);
        pendingIntent = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

        googleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        taskRepository = new TaskRepository(context);
    }

    @Override
    public void onLocationChanged(Location location) {
        lastKnownLocation = location;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "Google API Client has been connected");
        setLastKnownLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Google API Client connection failed");
    }

    @Override
    public void onResult(@NonNull Status status) {

    }

    public void connectApi() {
        googleApiClient.connect();
    }

    public void disconnectApi() {
        googleApiClient.disconnect();
    }

    public boolean checkPermission() {
        boolean result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (!result) ActivityCompat.requestPermissions(activity, new String[] { Manifest.permission.ACCESS_FINE_LOCATION }, REQUEST_PERMISSION);

        return result;
    }

    public void removeGeofences(List<String> geofenceIds) {
        if (geofenceIds.size() != 0) LocationServices.GeofencingApi.removeGeofences(googleApiClient, geofenceIds);
    }

    public void addGeofences(List<Geofence> geofences) {
        if (checkPermission()) LocationServices.GeofencingApi.addGeofences(googleApiClient, geofences, pendingIntent);
    }

    public void startLocationUpdates() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        if (checkPermission()) LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    private void setLastKnownLocation() {
        if (checkPermission()) {
            lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

            if (lastKnownLocation != null) {
                startLocationUpdates();
            } else {
                startLocationUpdates();
            }

        }

    }

    public Location getLastKnownLocation() {
        return lastKnownLocation;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setLastKnownLocation();
            } else {
                ArrayList<String> result = new ArrayList<>();

                AlertDialog.Builder dlg = new AlertDialog.Builder(context);
                dlg.create();
                dlg.setTitle(R.string.title_error);

                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) result.add(permissions[i]);
                }

                if (result.size() > 0) {
                    dlg.setMessage(context.getString(R.string.msg_required_permission, TextUtils.join(", ", result)));
                    dlg.show();
                }
            }
        }
    }

}