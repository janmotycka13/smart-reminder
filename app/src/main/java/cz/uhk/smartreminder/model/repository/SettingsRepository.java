package cz.uhk.smartreminder.model.repository;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;

import cz.uhk.smartreminder.model.repository.entities.Settings;
import cz.uhk.smartreminder.model.services.database.DatabaseHelper;

public class SettingsRepository {

    Context context;

    public SettingsRepository(Context context) {
        this.context = context;
    }

    public @Nullable Settings getSettings() {
        SQLiteDatabase db = new DatabaseHelper(context).getReadableDatabase();

        Cursor c = db.query(DatabaseHelper.TABLE_SETTINGS, new String[] { DatabaseHelper.COLUMN_ID, DatabaseHelper.COLUMN_RADIUS }, null, null, null, null, null);
        while (c.moveToNext()) {
            if (c.isLast()) return new Settings(c.getInt(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_ID)), c.getFloat(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_RADIUS)));
        }

        return null;
    }

    public void updateSettings(Settings editedSettings) {
        SQLiteDatabase db = new DatabaseHelper(context).getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.COLUMN_RADIUS, editedSettings.getRadius());

        db.update(DatabaseHelper.TABLE_SETTINGS, contentValues, null, null);

        db.close();
    }

}