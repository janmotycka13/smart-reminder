package cz.uhk.smartreminder.model.repository.entities;


public class Settings {

    int id;
    float radius;

    public Settings(int id, float radius) {
        this.id = id;
        this.radius = radius;
    }

    public int getId() {
        return id;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }
}
