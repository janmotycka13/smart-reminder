package cz.uhk.smartreminder.model.utils;

public class ArrayUtils {

    public static Integer[] stringArrToIntArr(String[] strings) {
        Integer[] arr = new Integer[strings.length];
        for (int i = 0; i < strings.length; i++) {
            arr[i] = Integer.valueOf(strings[i]);
        }

        return arr;
    }

}
