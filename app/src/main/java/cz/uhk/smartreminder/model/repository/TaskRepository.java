package cz.uhk.smartreminder.model.repository;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.smartreminder.model.repository.entities.Task;
import cz.uhk.smartreminder.model.services.database.DatabaseHelper;

public class TaskRepository {

    Context context;
    String[] projection = {DatabaseHelper.COLUMN_ID, DatabaseHelper.COLUMN_NAME, DatabaseHelper.COLUMN_NOTE, DatabaseHelper.COLUMN_LATITUDE, DatabaseHelper.COLUMN_LONGITUDE, DatabaseHelper.COLUMN_COMPLETED_DATE, DatabaseHelper.COLUMN_CREATED_DATE, DatabaseHelper.COLUMN_SEND_NOTIFICATION, DatabaseHelper.COLUMN_LIST_FK};

    public TaskRepository(@NonNull Context context) {
        this.context = context;
    }

    private List<Task> getTasks(@Nullable String selection, @Nullable String[] selectionArgs, @Nullable String groupBy, @Nullable String having, @Nullable String orderBy, @Nullable String limit) {
        SQLiteDatabase db = new DatabaseHelper(context).getReadableDatabase();

        List<Task> lstTasks = new ArrayList<>();
        Cursor c = db.query(DatabaseHelper.TABLE_TASK, projection, selection, selectionArgs, groupBy, having, orderBy, limit);
        while (c.moveToNext()) {
            Task t = new Task(
                    c.getInt(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_COMPLETED_DATE)),
                    c.getInt(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_CREATED_DATE)),
                    c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_NOTE)),
                    c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_NAME)),
                    c.getDouble(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_LATITUDE)),
                    c.getDouble(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_LONGITUDE)),
                    c.getInt(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_SEND_NOTIFICATION)) == 1,
                    c.getInt(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_LIST_FK)));

            t.setId(c.getInt(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_ID)));
            lstTasks.add(t);
        }

        c.close();
        db.close();

        return lstTasks;
    }

    public List<Task> getTasksByListId(@Nullable Integer listId) {
        String selection = listId == null ? null : DatabaseHelper.COLUMN_LIST_FK + " = ?";

        String[] selectionArgs = null;

        if (listId != null) {
            String[] sArgs = { String.valueOf(listId) };
            selectionArgs = sArgs;
        }

        return getTasks(selection, selectionArgs, null, null, DatabaseHelper.COLUMN_CREATED_DATE, null);
    }

    public List<Task> getTasksByGefenceIds(@NonNull String[] geofenceIds) {
        StringBuilder sb = new StringBuilder();
        sb.append("?");
        for (int i = 1; i < geofenceIds.length; i++) {
            sb.append(",?");
        }

        String selection = String.format("%s IN (%s)", DatabaseHelper.COLUMN_ID, sb.toString());

        return getTasks(selection, geofenceIds, null, null, DatabaseHelper.COLUMN_CREATED_DATE, null);
    }

    public List<Task> getTasksWithEnableSendingNotifications() {
        return getTasks(String.format("%s = ? AND %s = ?", DatabaseHelper.COLUMN_SEND_NOTIFICATION, DatabaseHelper.COLUMN_COMPLETED_DATE), new String[] { String.valueOf(1), String.valueOf(0) }, null, null, DatabaseHelper.COLUMN_CREATED_DATE, null);
    }

    public @Nullable Task getTaskById(@Nullable Integer taskId) {
        if (taskId == null) return null;

        List<Task> result = getTasksByGefenceIds(new String[] { taskId.toString() } );

        return result.size() == 0 ? null : result.get(0);
    }

    public List<Task> getCompletedTasks(@NonNull Integer taskId) {
        String selection = String.format("%s <> ? AND %s = ?", DatabaseHelper.COLUMN_COMPLETED_DATE, DatabaseHelper.COLUMN_LIST_FK);
        String[] selectionArgs = new String[] { String.valueOf(0), taskId.toString() };

        return getTasks(selection, selectionArgs, null, null, null, null);
    }

    public List<Task> getIncompletedTasks(@NonNull Integer taskId) {
        String selection = String.format("%s = ? AND %s = ?", DatabaseHelper.COLUMN_COMPLETED_DATE, DatabaseHelper.COLUMN_LIST_FK);
        String[] selectionArgs = new String[] { String.valueOf(0), taskId.toString() };

        return getTasks(selection, selectionArgs, null, null, null, null);
    }

    public void deleteTask(String taskId) {
        SQLiteDatabase db = new DatabaseHelper(context).getWritableDatabase();

        db.delete(DatabaseHelper.TABLE_TASK, String.format("%s = ?", DatabaseHelper.COLUMN_ID), new String[] { taskId });

        db.close();
    }

    /**
     * @param newTask
     * @return int new Task id
     */
    public int addTask(@NonNull Task newTask) {
        SQLiteDatabase db = new DatabaseHelper(context).getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.COLUMN_NAME, newTask.getName());
        contentValues.put(DatabaseHelper.COLUMN_NOTE, newTask.getNote());
        contentValues.put(DatabaseHelper.COLUMN_LATITUDE, newTask.getLatitude());
        contentValues.put(DatabaseHelper.COLUMN_LONGITUDE, newTask.getLongitude());
        contentValues.put(DatabaseHelper.COLUMN_COMPLETED_DATE, newTask.getCompletedDate());
        contentValues.put(DatabaseHelper.COLUMN_CREATED_DATE, newTask.getCreatedDate());
        contentValues.put(DatabaseHelper.COLUMN_SEND_NOTIFICATION, newTask.isSendNotification() ? 1 : 0);
        contentValues.put(DatabaseHelper.COLUMN_LIST_FK, newTask.getTaskListId());

        long insertedId = db.insert(DatabaseHelper.TABLE_TASK, null, contentValues);
        db.close();

        return (int) insertedId;
    }

    public void updateTask(@NonNull Task updatedTask) {
        SQLiteDatabase db = new DatabaseHelper(context).getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.COLUMN_NAME, updatedTask.getName());
        contentValues.put(DatabaseHelper.COLUMN_NOTE, updatedTask.getNote());
        contentValues.put(DatabaseHelper.COLUMN_LATITUDE, updatedTask.getLatitude());
        contentValues.put(DatabaseHelper.COLUMN_LONGITUDE, updatedTask.getLongitude());
        contentValues.put(DatabaseHelper.COLUMN_COMPLETED_DATE, updatedTask.getCompletedDate());
        contentValues.put(DatabaseHelper.COLUMN_CREATED_DATE, updatedTask.getCreatedDate());
        contentValues.put(DatabaseHelper.COLUMN_SEND_NOTIFICATION, updatedTask.isSendNotification() ? 1 : 0);

        db.update(DatabaseHelper.TABLE_TASK, contentValues, String.format("%s = ?", DatabaseHelper.COLUMN_ID), new String[] { String.valueOf(updatedTask.getId()) });
        db.close();
    }

}
