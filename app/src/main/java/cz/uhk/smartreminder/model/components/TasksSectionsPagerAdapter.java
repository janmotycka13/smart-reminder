package cz.uhk.smartreminder.model.components;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import java.io.Serializable;

import cz.uhk.smartreminder.activities.MainActivity;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class TasksSectionsPagerAdapter extends FragmentPagerAdapter implements Serializable {

    String[] titles;
    int taskListId;

    public TasksSectionsPagerAdapter(FragmentManager fm, String[] titles, int taskListId) {
        super(fm);
        this.titles = titles;
        this.taskListId = taskListId;
    }

    @Override
    public Fragment getItem(int position) {
        return TasksPlaceholderFragment.newInstance(position, taskListId);
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position - 1];
    }
}
