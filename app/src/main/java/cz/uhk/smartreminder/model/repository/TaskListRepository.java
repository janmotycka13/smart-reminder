package cz.uhk.smartreminder.model.repository;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.smartreminder.model.repository.entities.Task;
import cz.uhk.smartreminder.model.repository.entities.TaskList;
import cz.uhk.smartreminder.model.services.database.DatabaseHelper;

public class TaskListRepository {

    Context context;
    String[] projection = new String[] { DatabaseHelper.COLUMN_ID, DatabaseHelper.COLUMN_NAME, DatabaseHelper.COLUMN_CREATED_DATE };

    public TaskListRepository(Context context) {
        this.context = context;
    }

    private List<TaskList> getTasksLists(@Nullable String selection, @Nullable String[] selectionArgs, @Nullable String groupBy, @Nullable String having, @Nullable String orderBy, @Nullable String limit) {
        TaskRepository taskRepository = new TaskRepository(context);
        SQLiteDatabase db = new DatabaseHelper(context).getReadableDatabase();

        List<TaskList> lstTaskLists = new ArrayList<>();

        Cursor c = db.query(DatabaseHelper.TABLE_TASK_LIST, projection, selection, selectionArgs, groupBy, having, orderBy, limit);

        while (c.moveToNext()) {
            Integer id = c.getInt(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_ID));
            TaskList taskList = new TaskList(id, c.getInt(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_CREATED_DATE)), c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_NAME)));
            taskList.setLstTasks(taskRepository.getTasksByListId(id));
            lstTaskLists.add(taskList);
        }

        c.close();
        db.close();

        return lstTaskLists;
    }

    public List<TaskList> getTaskLists() {
        return getTasksLists(null, null, null, null, null, null);
    }

    public @Nullable TaskList getTaskListById(int id) {
        String selection = DatabaseHelper.COLUMN_ID + " = ?";
        String[] selectionArgs = { String.valueOf(id) };

        List<TaskList> tasksLists = getTasksLists(selection, selectionArgs, null, null, null, null);

        return tasksLists.size() == 0 ? null : tasksLists.get(0);
    }

    public void deleteTaskListById(int id) {
        SQLiteDatabase db = new DatabaseHelper(context).getWritableDatabase();

        String selection = DatabaseHelper.COLUMN_ID + " = ?";
        String[] selectionArgs = { String.valueOf(id) };

        db.delete(DatabaseHelper.TABLE_TASK_LIST, selection, selectionArgs);
        db.close();
    }

    public void insertTaskList(TaskList taskList) {
        SQLiteDatabase db = new DatabaseHelper(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_NAME, taskList.getName());
        values.put(DatabaseHelper.COLUMN_CREATED_DATE, taskList.getCreatedDateAsInteger());

        db.insert(DatabaseHelper.TABLE_TASK_LIST, null, values);
        db.close();
    }

    public void updateTaskList(TaskList taskList) {
        SQLiteDatabase db = new DatabaseHelper(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_NAME, taskList.getName());

        String selection = DatabaseHelper.COLUMN_ID + " = ?";
        String[] selectionArgs = { String.valueOf(taskList.getId()) };

        db.update(DatabaseHelper.TABLE_TASK_LIST, values, selection, selectionArgs);
        db.close();
    }

}
