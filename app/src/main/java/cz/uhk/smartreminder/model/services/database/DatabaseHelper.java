package cz.uhk.smartreminder.model.services.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "smart-reminder";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE_TASK_LIST = "TaskList";
    public static final String TABLE_TASK = "Task";
    public static final String TABLE_SUBTASK = "Subtask";
    public static final String TABLE_SETTINGS = "Settings";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_CREATED_DATE = "createdDate";
    public static final String COLUMN_NOTE = "note";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE = "longitude";
    public static final String COLUMN_COMPLETED_DATE = "completedDate";
    public static final String COLUMN_SEND_NOTIFICATION = "sendNotification";
    public static final String COLUMN_LIST_FK = "fk_list";
    public static final String COLUMN_TASK_FK = "fk_task";
    public static final String COLUMN_RADIUS = "radius";
    public static final float DEFAULT_RADIUS = 500.0f;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(String.format("CREATE TABLE %s " +
                "(" +
                "%s INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "%s TEXT NOT NULL, " +
                "%s INTEGER NOT NULL);", TABLE_TASK_LIST, COLUMN_ID, COLUMN_NAME, COLUMN_CREATED_DATE));

        db.execSQL(String.format("CREATE TABLE %s " +
                "(" +
                "%s INTEGER NOT NULL PRIMARY KEY, " +
                "%s TEXT, " +
                "%s TEXT NOT NULL, " +
                "%s REAL NOT NULL, " +
                "%s REAL NOT NULL, " +
                "%s INTEGER DEFAULT 0, " +
                "%s INTEGER NOT NULL, " +
                "%s INTEGER NOT NULL DEFAULT 1, " +
                "%s INTEGER NOT NULL, " +
                "CONSTRAINT FK_Task_List FOREIGN KEY (%s) REFERENCES %s (%s) ON DELETE Cascade ON UPDATE No Action );", TABLE_TASK, COLUMN_ID, COLUMN_NOTE, COLUMN_NAME, COLUMN_LATITUDE, COLUMN_LONGITUDE, COLUMN_COMPLETED_DATE, COLUMN_CREATED_DATE, COLUMN_SEND_NOTIFICATION, COLUMN_LIST_FK, COLUMN_LIST_FK, TABLE_TASK_LIST, COLUMN_ID));

        db.execSQL(String.format("CREATE TABLE %s " +
                "(" +
                "%s INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "%s TEXT NOT NULL, " +
                "%s INTEGER NOT NULL, " +
                "%s INTEGER, " +
                "%s INTEGER NOT NULL, " +
                "CONSTRAINT FK_Subtask_Task FOREIGN KEY (%s) REFERENCES %s (%s) ON DELETE Cascade ON UPDATE No Action );", TABLE_SUBTASK, COLUMN_ID, COLUMN_NAME, COLUMN_CREATED_DATE, COLUMN_COMPLETED_DATE, COLUMN_TASK_FK, COLUMN_TASK_FK, TABLE_TASK, COLUMN_ID));

        db.execSQL(String.format("CREATE TABLE %s " +
                "(" +
                "%s  INTEGER NOT NULL PRIMARY KEY, " +
                "%s  REAL NOT NULL );", TABLE_SETTINGS, COLUMN_ID, COLUMN_RADIUS));

        db.execSQL(String.format("CREATE INDEX 'IXFK_Task_List' ON %s ('%s' ASC);", TABLE_TASK, COLUMN_LIST_FK));
        db.execSQL(String.format("CREATE INDEX 'IXFK_Subtask_Task' ON %s ('%s' ASC);", TABLE_SUBTASK, COLUMN_TASK_FK));

        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_RADIUS, DEFAULT_RADIUS);

        db.insert(TABLE_SETTINGS, null, contentValues);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(String.format("DROP TABLE IF EXISTS %s;", TABLE_SETTINGS));
        db.execSQL(String.format("DROP TABLE IF EXISTS %s;", TABLE_SUBTASK));
        db.execSQL(String.format("DROP TABLE IF EXISTS %s;", TABLE_TASK));
        db.execSQL(String.format("DROP TABLE IF EXISTS %s;", TABLE_TASK_LIST));
        onCreate(db);
    }

}
