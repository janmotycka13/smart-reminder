    Jedná se o chytrý location reminder, který bude uživateli připomínat úkoly na základě lokality, kde se nachází.
Uživatel si tedy předem stanoví, kde a co potřebuje pomocí interních Google Map a poté jestliže se dostane do určitého radiusu kolem 
předem uloženého bodu, vyskočí pomocí push notifikaci upozornění a podrobnosti o úkolu. Aplikace bude tedy využívat GPS dat.
Aplikace bude sestavena jak pomocí již integrovaného Geofence api, tak vlastní implementace geofence a poté budou mezi sebou porovnávány například na základě přesnosti, rychlosti reakce nebo vlivu na výdrž baterie. 
Aplikace bude využívat i accelerometru pro kontrolu, zda se uživatel pohybuje.
    Písemná práce i implementace aplikace se bude opírat o článek - https://link.springer.com/article/10.1007/s00779-013-0646-2 a další články týkající se problematiky gps, geofencingu.